Am 23.10.2023 fand ein Workshop zum Thema Geschäftsmodelle statt. Die Bedingung für die Teilnahme war die Einreichung eines Konzeptpapiers über die Issue-Funkiton.
Alle eingereichten Konzeptpapiere können Sie sich in der Issue-Übersicht mithilfe des Filters Label=Workshop Submissions_Business_and_Issuance_Models_10/23 anzeigen lassen.

A workshop on the topic of business models took place on 23.10.2023. The condition for participation was the submission of a concept paper via the issue function.
You can view all submitted concept papers in the issue overview using the filter Label=Workshop Submissions_Business_and_Issuance_Models_10/23.


Issueboard-Link to all submissions: https://gitlab.opencode.de/bmi/eidas2/-/boards?label_name[]=Workshop%20Submissions_Business_and_Issuance_Models_10%2F23