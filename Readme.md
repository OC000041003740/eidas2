# eIDAS2

Information zu Zielen, Rahmenbedingungen, ersten Überlegungen sowie zum Start des öffentlichen Architektur- & Konsultationsprozesses zu einem Konzept der deutschen Ausgestaltung von EUDI-Wallets.

# eIDAS-Novellierung legt Grundlagen, Vorraussetzungen und Anforderungen für EUDI-Wallets fest

> English version below

Am 29. Februar 2024 hat das Europäische Parlament in einer finalen Abstimmung die novellierte eIDAS-Verordnung (eIDAS 2.0) verabschiedet. Im Folgenden werden einzelne Aspekte zur EUDI-Wallet aus der novellierten eIDAS-Verordnung beschrieben. **Die Aufzählung ist nicht abschließend und wird laufend ergänzt.** Für ein vollständiges Verständnis wird die Lektüre des finalen Verordnungstexts, [wie vom Parlament verabschiedet](https://www.europarl.europa.eu/doceo/document/A-9-2023-0038-AM-006-006_DE.pdf), empfohlen. Das Dokument soll als Lesehilfe und Nachschlagwerk für das Verständnis der Verordnung unterstützen.

1. **Grundlagen der EUDI-Wallet**, Art. 5a
	
	1. „Eine Europäische Brieftasche für die Digitale Identität ist ein **elektronisches Identifizierungsmittel**, das es dem Nutzer ermöglicht, **Personenidentifizierungsdaten** und **elektronische Attributsbescheinigungen** (d.h. weitere Nachweise) sicher zu **speichern**, zu **verwalten und zu validieren**, um sie vertrauenden Beteiligten (also z.B. Behörden oder Unternehmen) und anderen Nutzern von europäischen Brieftaschen für die Digitale Identität zu **präsentieren** und mittels qualifizierter elektronischer Signaturen zu **unterzeichnen** oder mittels qualifizierter elektronischer Siegel zu **besiegeln**“ (Art. 3, 42). Dazu gehört auch die Kombination verschiedener Nachweise (Art. 5a, 4). Die Anwendung soll **online wie auch offline** möglich sein (ebd.).
	2. Mindestens eine EUDI-Wallet muss für Bürgerinnen und Bürger sowie für Organisationen **durch ihren Mitgliedsstaat** spätestens zwei Jahre nach Inkrafttreten bestimmter Durchführungsrechtsakte **bereitgestellt bzw. anerkannt werden** (vrstl. Q1 2027), (Art. 5a, 1 sowie Art. 5a, 2).
	3. Die Nutzung einer EUDI-Wallet muss für Bürgerinnen und Bürger **kostenlos** (Art. 5a, 12) und **freiwillig** sein (Art. 5a, 15).
	4. Das Onboarding der Nutzenden auf eine EUDI-Wallet und die Bereitstellung einer solchen soll auf dem **Vertrauensniveau Hoch** erfolgen (Art. 5a, 11).
	5. **Ausschluss von Tracking** und Sicherstellung von **Unlinkability** von Nutzer-Verhalten, auch durch Herausgeber von Nachweisen und durch die Diensteanbieter (Art. 5a, Absatz 5, b; Art. 5a, 14 sowie Art, 5a, 16).
	6. „Für den Quellcode der Anwendungssoftwarekomponenten von europäischen Brieftaschen für die Digitale Identität muss eine **Open-Source-Lizenz** gelten.“ (Art. 5a, 3, Satz 1)
	7. Eine EUDI-Wallet muss allen natürlichen Personen die Möglichkeit bieten, mittels qualifizierter elektronischer Signaturen (Dokumente) **kostenlos zu unterzeichnen**. (Art. 5a, 5, g) – mit der Möglichkeit einer Beschränkung der kostenlosen Signaturen auf nichtgewerbliche Zwecke (ebd.).
	8. Von staatlicher Seite sollen folgende Informationen in einer EUDI-Wallet bestätigt/bereitgestellt werden (Anhang 6): Adresse, Alter, Geschlecht, Personenstand, Familienzusammensetzung, Staatsangehörigkeit oder Staatsbürgerschaft, Bildungsabschlüsse, Titel und Erlaubnisse.

2. **Voraussetzungen für Diensteanbieter** (in der VO „vertrauende Beteiligte“ genannt, Art. 5b)

	1. Um auf eine EUDI-Wallet zugreifen zu können, muss ein Diensteanbieter sich **in dem Mitgliedstaat registrieren**, wo er niedergelassen ist.
	2. Das **Registrierungsverfahren** muss **kosteneffizient** und dem Risiko **angemessen** sein. Der Diensteanbieter gibt dabei **Mindestinformationen** an (z.B. Name, Kontaktangaben, beabsichtigte Verwendung der EUDI-Wallet, einschließlich der Daten, die von den Nutzern angefordert werden soll).
	3. Diensteanbieter dürfen nur die Daten aus eine EUDI-Wallet abrufen, für die sie sich registriert haben (Art. 5b, Absatz 3) und müssen sich **gegenüber dem Nutzer selbst identifizieren** bzw. authentifizieren bevor sie Daten abrufen (Art. 5a, Absatz 5c).
	4. Diensteanbieter dürfen **die Verwendung von Pseudonymen** nicht verweigern, wenn die Identifizierung des Nutzers nicht im Unionsrecht oder im nationalen Recht vorgeschrieben ist.
	5. Vermittler, die im Namen von Diensteanbietern handeln, sind als vertrauende Beteiligte zu betrachten und dürfen **keine Daten über den Inhalt der Transaktion speichern** (Art. 5b, Abs. 10).
	6. Diensteanbieter müssen **die Echtheit und Gültigkeit von europäischen Brieftaschen** für die Digitale Identität überprüfen (Art. 5, Absatz 5, a, viii).

3. **Anerkennung einer EUDI-Wallet im (grenzübergreifenden) Geschäftsverkehr** (Art. 5f)

	1. Der Zugang zu einem **von einer öffentlichen Stelle erbrachten Online-Dienst**, der eine elektronische Identifizierung und Authentifizierung erfordert, **muss über jede EUDI-Wallet ermöglicht werden**, die in einem der Mitgliedstaaten anerkannt wurde.
	2. **Private Organisationen** (Ausnahme von Kleinst- und Kleinunternehmen), die Dienste erbringen, und dabei rechtlich verpflichtet sind, eine (starke) Online-Identifizierung vorzunehmen, oder diese vertraglich vorgeschrieben ist, müssen auf das freiwillige Verlangen des Nutzers **eine EUDI-Wallet akzeptieren**. Bspw. in den Bereichen Verkehr, Banken, Finanzdienstleistungen, Gesundheit, Postdienste, Bildung, Telekommunikation, etc. (spätestens 36 Monate nach Inkrafttreten bestimmter Durchführungsrechtsakte; vrst. Q1 2028).
	3. **Sehr große Onlineplattformen**, die für den Zugang zu Online-Diensten eine Nutzerauthentifizierung erfordern (z.B. Login bei Facebook, Google, Microsoft, etc.) müssen auf das freiwillige Verlangen des Nutzers **eine EUDI-Wallet akzeptieren**.


 # eIDAS Amendment Sets Out Principles, Prerequisites and Requirements for EUDI Wallets

 On 29th February 2024, the European Parliament adopted the amended eIDAS Regulation (eIDAS 2.0) in a final vote. The following text describes specific aspects of the EUDI Wallet as outlined in the amended eIDAS Regulation. Please note that this list is not exhaustive and will be continuously updated. For a comprehensive understanding, we recommend reading the final text of the regulation [as adopted by Parliament](https://www.europarl.europa.eu/doceo/document/A-9-2023-0038-AM-006-006_EN.pdf). The document is intended to serve as a reading aid and reference guide to support understanding of the Regulation.

1. **Basics of the EUDI Wallet**, Art. 5a

	1. “‘European Digital Identity Wallet’ means an **electronic identification means** which allows the user to securely **store, 	manage and validate person identification data** and **electronic attestations** of attributes for the purpose of providing them to relying parties and other users of European Digital Identity Wallets, and to sign by means of qualified electronic signatures or to seal by means of qualified electronic seals.” (Art. 3, 42). This also includes the **combination** of different forms of proof (Art. 5a, 4). The application should be possible **both online and offline** (ibid.).
	2. At least one EUDI Wallet must be **made available or recognised** for citizens and organisations **by their Member State** no later than two years after the entry into force of certain implementing acts **(expected Q1 2027)** (Art. 5a, 1 and Art. 5a, 2).
	3. The use of an EUDI Wallet must be **free of charge** for citizens (Art. 5a, 12) and **voluntary** (Art. 5a, 15).
	4. The onboarding of users to an EUDI Wallet and the provision of such a wallet should take place at **trust level “high”** (Art. 5a, 11).
	5. **Exclusion of tracking** and ensuring **unlinkability** of user behaviour, including by publishers of evidence and by service providers (Art. 5a, para. 5, b; Art. 5a, 14 and Art. 5a, 16).
	6. "The source code of the application software components of European Digital Identity Wallets shall be **open-source licensed."** (Art. 5a, 3, sentence 1)
	7. An EUDI Wallet must offer all natural persons the possibility to **sign** (documents) **free of charge** by means of qualified electronic signatures (Art. 5a, 5, g) - with the possibility of restricting free signatures to non-commercial purposes (ibid.)
	8. The following information should be confirmed/provided by the state in an EUDI Wallet (Annex 6): address, age, gender, marital status, family composition, nationality or citizenship, educational qualifications, titles, and authorisations.


2. **Requirements for service providers** (referred to as "relying parties" in the Regulation, Art. 5b)

	1. In order to access an EUDI Wallet, a service provider must **register in the Member State** in which it is established.
	2. The **registration process** must be **cost-effective** and **proportionate** to the risk. The service provider shall provide **minimum information** (e.g., name, contact details, intended use of the EUDI Wallet, including the data to be requested from users).
	3. Service providers may only retrieve data from an EUDI Wallet for which they have registered (Art. 5b, para. 3) and must **identify** or authenticate **themselves to the user** before retrieving data (Art. 5a, para. 5c).
	4. Service providers may not refuse **the use of pseudonyms** if the identification of the user is not required by Union or national law.
	5. Intermediaries acting on behalf of service providers are to be regarded as relying parties and may **not store any data on the content of the transaction** (Art. 5b, para. 10).
	6. Service providers must verify **the authenticity and validity of European** Digital Identity **Wallets** (Art. 5, para. 5, a, viii).

3. **Recognition of an EUDI Wallet in (cross-border) business transactions** (Art. 5f)

	1. Access to an **online service provided by a public sector body** that requires electronic identification and authentication **must be possible via any EUDI Wallet** recognised in one of the Member States.
	2. **Private organisations** (with the exception of micro and small enterprises) that provide services and are legally obliged to carry out (strong) online identification or are contractually obliged to do so must **accept an EUDI Wallet** at the **voluntary request of the user**, e.g., in the areas of transport, banking, financial services, health, postal services, education, telecommunications, etc. (at the latest 36 months after the entry into force of certain implementing acts; pr. Q1 2028).
	3. **Very large online platforms** that require user authentication to access online services (e.g., login to Facebook, Google, Microsoft, etc.) **must accept an EUDI Wallet** at the voluntary request of the user.

# Aktuelle Zeitplanung des Architektur- & Konsultationsprozesses

Die **Kick-Off Veranstaltung** des Architektur- & Konsultationsprozesses fand am **31.07.2023** statt. Weitere Informationen finden Sie [**hier**](https://gitlab.opencode.de/bmi/eidas2/-/tree/main/230703_Kick_Off_Konsultationsprozess?ref_type=heads).

Am **30.08.2023** wurde in einem ersten Workshops gemeinschaftlich mit rund 40 Teilnehmenden an der Identifizierung relevanter **Use Cases** für EUDI-Wallets gearbeitet. Eine Zusammenfassung der Ergebnisse sowie die Videoaufzeichnung stehen [**hier**](https://gitlab.opencode.de/bmi/eidas2/-/tree/main/230830_Workshop_Use_Cases?ref_type=heads) bereit.

Der zweite Workshop **"Geschäfts- und Betriebsmodelle für privat betriebene EUDI-Wallets"** fand am **23.10.2023** mit rund 30 Teilnehmenden in Berlin statt. Eine Videoaufzeichnung gibt es [**hier**](https://gitlab.opencode.de/bmi/eidas2/-/tree/main/Business_and_Issuance_Models/01_Outcome_Workshop/Recordings).

Bei der **ersten Offenen Online-Sprechstunde am 08.02.2024** mit rund 80 Teilnehmenden stand der veröffentlichte **erste Architektur-Vorschlag** im Mittelpunkt. Die entsprechende Videoaufzeichnung kann [**hier**](https://gitlab.opencode.de/bmi/eidas2/-/tree/main/Architecture%20Consultation/Consultation%2008.02.2024) abgerufen werden.

**Weitere Konsultationsformate** unter anderem zum Thema Datenschutz und Datensparsamkeit sind **für 2024 geplant**. Angaben zu genauen Terminen und Möglichkeiten der Teilnahme werden rechtzeitig auf OpenCoDE veröffentlicht.  

# Antrag einreichen
Bitte nutzen Sie die "Issue"-Funktion, um einen Antrag einzureichen.

Hierbei können Dokumente per Drag&Drop hinzugefügt werden.

![Einreichen](Antragseinreichung.gif)
