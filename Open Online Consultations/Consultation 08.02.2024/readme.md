## Hinweis zur Offenen Online-Sprechstunde vom 08.02.2024

Beantwortete Fragen in der offenen Sprechstunde / Architektur Proposal (08.02.2024)
Experte: Dr. Daniel Fett


Sie finden anbei die Timecodes der Beantwortung der Fragen:

Fragencluster

1.	Allgemein


o	von André Röder KAPRION an alle:    4:16 PM

Der Architekturentwurf lässt eine starke Verankerung zu den OpenID4VC-Spezifikationen erkennen. Welche Maßnahmen sind vorgesehen, um eine Migrationsfähigkeit zu weiterführenden Entwicklungen wie KERI (see GLEIF) oder DIDcomm_v2 in Zukunft zu ermöglichen?

Beantwortet: 
part_2 10:20-11:39



o	von Andreas Plies - CEO AUTHADA an alle:    4:23 PM

Wird es eine Unterscheidung von präferierten Varianten für iOS und Android geben? Es sind eben unterschiedliche technologische Möglichkeiten vorhanden. Oder setzt man auf eSIMS?

Beantwortet: 
part_3 01:13-02:12



o	von Ralf Knobloch an alle:    4:16 PM

Wenn Sie Register sagen, was für Register meinen Sie?

Beantwortet: 
part_2 10:00-10:20



2.	Optionen aus dem AP

2.1	Option B

•	von Clormann, Dr. Michael an alle:    4:12 PM

Inwieweit wird die im Architekturproposal skizzierte "Option B" als Übergangs- oder Dauerlösung für die Implementierung der EUdi-Wallet gesehen? Wie wird diese Option hinsichtlich ihrer eIDAS-Konformität bewertet?

Beantwortet: 
part_2 06:38-08:54


2.2	Option C

•	von André Röder KAPRION an alle:    4:21 PM

Ist in Option C eine Präsentation Mobile-to-Mobile vorgesehen?

Beantwortet: 
part_2.mp4 20:39-20:58





•	von Gregor Bransky [InÖG] he/him an alle:    4:23 PM

Ist nicht in Option C für LoA hoch der Bruch der Usability ein Security Feature sondern schafft entsprechende Sicherheit?
	
Beantwortet: 
part_2.mp4 21:04-Ende und part_3.mp4  00:00 – 01:08
(durch Wechsel der Datei fehlen vereinzelte Inhalte in der Detaillierung der Frage.)


2.3	Option D


•	von Patrick an alle:    4:20 PM

AUTHADA hat Option D1.2 schon umgesetzt. Unsere Lösung skaliert auch auf alle Samsunggeräte mit eSE. Gibt es Präferenz welche Option es wird? Ist eine Kombination aus 2 Lösungen möglich? bspw. Option B für Geräte ohne eSE und Option D für Geräte mit.

Beantwortet: 
part_2 12:57-14:15



3.	Authentifizierung/LoA 

•	von Ionita, Andrei an alle:    4:21 PM

Auf welche Definition von LoA wird hier verwiesen? Was heißt LoA high konkret?

Beantwortet: 
part_2.mp4: 18:49-20:35



4.	Datenschutz

•	von Armin Bauer IDnow an alle:    4:25 PM

Wo genau liegt das Problem mit einer Issuer-Signatur? Alle anderen Staaten scheinen diesen Weg zu gehen. Wo genau liegt das Problem eines Leaks von signierten Daten bei einer Bank im Vergleich wenn die Bank unsignierte Daten leakt?


Beantwortet 
part_3.mp4: 05:58-08:47


5.	PID 


•	von Matthias Fuhrland an alle:    4:14 PM

Welche Rolle sollen die Kommunen beim Ausstellungsprozess der PID spielen?

Beantwortet: 
part_2 8:54-10:00


•	von André Röder KAPRION an alle:    4:25 PM

Ansatz D 
Wie soll der schreibende Zugriff für eine Vielzahl von Issuern realisiert werden?

Beantwortet: 
part_3.mp4: 03:03-03:37
 
Wie ist der Umzug des Bürgers von einem EU-Gliedstaat in einen anderen vorgesehen? 

Beantwortet: 
part_3.mp4: 03:37-04:38

Ist je Gliedstaat nur ein PID-Provider vorgesehen?

Beantwortet:
part_3.mp4: 04:42-05:52


6.	RP / Use Cases

•	von Matthias Fuhrland an alle:    4:18 PM

Ihr Architekturkonzept auf Basis von OpenID eignet sich zum Abbilden eines einzelnen Prozessschrittes (Authentifizierung), aber nicht zur Abbildung von Geschäftsprozessen, die den Austausch von Credentials in beide Richtungen beinhalten. Nun soll ja Ende 2024 das Konzept Gesetzeskraft erlangen. Wurde ein echter Geschäftsprozess schon mal in einer kommunalen Produktivumgebung erprobt?  

Beantwortet:
part2.mp4 11:40-12:55


Welche Informationen in und zu Credentials sind mit Ihrer Architektur automatisiert überprüfbar (Vertrauensfragen)?

Beantwortet: 
part2.mp4 14:26-18:44




•	von I. Radatz HSH an alle:    4:12 PM

Ist es geplant, neben eID-Daten die Quelldaten-Fachregistern (Melde~, Gewerbe~, KfZ~) zu nutzen?

Beantwortet: 
•	part_2 5:18-06:36



